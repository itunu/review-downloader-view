import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CONSTANTS} from '../common/constants'
import {ReviewService} from "../services/review.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  searchUrlForm : FormGroup;
  downloadUrl;
  text = 'Download';
  buttonText = 'Submit';
  errorMsg = '';

  constructor(
    public _formBuilder:FormBuilder,
    public _reviewService:ReviewService
  ) {
    this.searchUrlForm = this._formBuilder.group({
      searchUrl : ['', [Validators.required, Validators.pattern('https?://.+')]],
    });
  }

  ngOnInit() {
  }

  isFieldValid(field: string) {
    console.log(this.searchUrlForm.get(field));
    return !this.searchUrlForm.get(field).valid &&
           this.searchUrlForm.get(field).touched;
  }

  displayError(field: string) {
    return {
      'has-success': this.searchUrlForm.get(field).valid,
      'has-warning':this.isFieldValid(field)
    }
  }

  onSubmit() {
    this.downloadUrl = '';
    this.errorMsg = '';
    this.text = 'Download';
    let searchUrl = this.searchUrlForm.value.searchUrl;
    if (searchUrl.includes(CONSTANTS.EXPECTED_URLS.YELP)) {
      this.handleYelpReview(searchUrl);
    } else if (searchUrl.includes(CONSTANTS.EXPECTED_URLS.GOOGLE)) {
      this.handleGoogleApiReview(searchUrl);
    }
  }

  handleYelpReview(searchUrl) {
    let id = searchUrl.slice(CONSTANTS.EXPECTED_URLS.YELP.length);
    if (id) {
      let url = `${CONSTANTS.API_URL}/${id}`;
      this.verifyUrl(url);
    }
  }

  onLinkClick() {
    this.text = 'Downloading';
  }

  handleGoogleApiReview(searchUrl) {
      let url = searchUrl.slice(CONSTANTS.EXPECTED_URLS.GOOGLE.length).split('/');
      let tempLocation = url[1].slice(1).split(',');
      let location = `${tempLocation[0]},${tempLocation[1]}`;
      let keyword = url[0];
      if(keyword && location) {
        let url = `${CONSTANTS.API_URL}/google/${location}/${keyword}`;
        this.verifyUrl(url);
      }
  }

  verifyUrl(url) {
    this.buttonText = 'Loading...';
    this._reviewService.verify(url).then((response)=> {
      this.buttonText = 'Submit';
      this.text = 'Download';
      if(response && response.status === 'error') {
        this.errorMsg = 'We couldn\'t find the requested place or business';
      }
    }).catch((err) => {
      this.text = 'Download';
      this.buttonText = 'Submit';
      this.downloadUrl = url;
    });
  }

}
