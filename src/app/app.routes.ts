/**
 * Created by itunu on 1/11/18.
 */
import { Routes, RouterModule } from '@angular/router';
import {NgModule} from "@angular/core";
import {HomeComponent} from "./home/home.component";


const appRoutes: Routes = [
  { path: '', redirectTo:'home', pathMatch:'full' },
  { path: 'home', component:HomeComponent },

];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
    // other imports here
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
export const routedComponents = [
  HomeComponent
];
