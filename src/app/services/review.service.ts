import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {CONSTANTS} from '../common/constants'

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ReviewService {

  constructor(private http:Http) { }

  verify(url) {
    const headers = new Headers();
    return this.http.get(url,{headers:headers}).toPromise<any>()
      .then(response => Promise.resolve(response.json()))
      .catch( error =>  Promise.reject(error.message || error));
  }


}
